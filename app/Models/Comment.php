<?php

namespace App\Models;

use App\Events\CommentWritten;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Support\Facades\Event;

class Comment extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'body',
        'user_id'
    ];

    /**
     * Get the user that wrote the comment.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function addNewComment($userId){
        $newComment = $this->create(['user_id' => $userId,'body' => 'New Comment']);
        
        
        return $newComment;
    }

    public function getUserComment($userId){
        return $this->where('user_id', $userId)->get();
    }
}
