<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;

use App\Events\AchievementUnlocked;
use App\Events\BadgeUnlocked;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    public $lessonAchievements = [
        "First Lesson Watched" => 0,
        "10 Lessons Watched" => 10,
        "25 Lessons Watched" => 25,
        "50 Lessons Watched" => 50
    ];
    
    public $commentAchievements = [
        "First Comment Written" => 0,
        "10 Comments Written" => 10,
        "25 Comments Written" => 25,
        "50 Comments Written" => 50
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
        'password' => 'hashed',
    ];

    /**
     * The comments that belong to the user.
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * The lessons that a user has access to.
     */
    public function lessons()
    {
        return $this->belongsToMany(Lesson::class);
    }

    /**
     * The lessons that a user has watched.
     */
    public function watched()
    {
        return $this->belongsToMany(Lesson::class)->wherePivot('watched', true);
    }

    /**
     * Get User data, lesson and comments by user id
     */
    public function getUserDataWithLessonAndComments($id){
        $userData = User::with('comments', 'lessons')->find($id);
        
        return $userData;
        
    }

    /**
     * Get List of All Available Achievements Combined
     */
    public function getAllAvailableAchievements(){
        
        $allAchievements = array_merge(array_keys($this->lessonAchievements),array_keys($this->commentAchievements));
        return $allAchievements ;
    }

    /**
     * Get List of Unlockable Badges with Achievement Threshold
     */
    public function getAllUnlockableBadgesWithAchievementThresholds(){
        $unlockbleBadgesWithAchievementThresholds = [
            'Beginner' => 0,
            'Intermediate' => 4,
            'Advanced' => 8,
            'Master' => 10,
        ];
        return $unlockbleBadgesWithAchievementThresholds ;
    }

    /**
     * Get List of All Unlockable Badges
     */
    public function getAllUnlockableBadges(){
        $unlockbleBadgesWithAchievementThresholds = $this->getAllUnlockableBadgesWithAchievementThresholds();
        $unlockbleBadgesNames = array_keys($unlockbleBadgesWithAchievementThresholds);
        return $unlockbleBadgesNames ;
    }

    
    /**
     * Get List of Unlocked Achievement
     */
    public function getUnlockedAchievementsList($userDataWithLessonAndComments){

        $comments = $userDataWithLessonAndComments->comments;
        $lessons = $userDataWithLessonAndComments->lessons;

        $commentsCount = $comments->count();
        $lessonCount = $lessons->count();

        $lessonAchievementsWithThreshold = $this->lessonAchievements;
        $commentAchievementsWithThreshold = $this->commentAchievements;

        $userCommentAchievements = $this->calculateUnlockedAchievement($commentAchievementsWithThreshold,$commentsCount);
        $userLessonAchievements = $this->calculateUnlockedAchievement($lessonAchievementsWithThreshold,$lessonCount);

        //dd($lessonAchievements,$commentAchievements);
        return [
            'userLessonAchievements' => $userLessonAchievements,
            'userCommentAchievements' => $userCommentAchievements,
        ];
    }

    /**
     * Calculate Achievement unlocked by user based on user's achievement count
     */
    public function calculateUnlockedAchievement($achievementsWithThreshold,$achievementCount){
        $userAchievements = array();
        foreach ($achievementsWithThreshold as $achievement => $threshold) {
            if ($achievementCount > $threshold) {
                array_push($userAchievements,$achievement);
            }
        }
        return $userAchievements ;
        
    }
    /**
     * Get Next available Achievement
     */
    public function getNextAvailableAchievements($userDataWithLessonAndComments){
        $allAvailableAchievements = $this->getAllAvailableAchievements();
        $allUnlockedAchievements = $this->getUnlockedAchievementsList($userDataWithLessonAndComments);
        //dd($allUnlockedAchievements);
        
        $combinedUnlockedAchievements = array_merge($allUnlockedAchievements['userLessonAchievements'],$allUnlockedAchievements['userCommentAchievements']);
        //dd($combinedUnlockedAchievements);

        $nextAvailableAchievements = array_diff($allAvailableAchievements, $combinedUnlockedAchievements);
        
        return $nextAvailableAchievements ;
    }
    
    /**
     * Check of user has unlocked any badge
     */
    public function checkBadgeUnlocked($userDataWithLessonAndComments){
        $getUnlockedAchievementsList = $this->getUnlockedAchievementsList($userDataWithLessonAndComments);
        $totalAchievements = sizeof($getUnlockedAchievementsList);
        
        // Check if the user has earned a new badge
        if ($totalAchievements == 4 || $totalAchievements == 8 || $totalAchievements == 10) {
            return true;
        }
        return false;
    }

    /**
     * Calculate Badge earned by user based on user's achievement count
     */
    public function calculateBadge($userDataWithLessonAndComments){
        $getUnlockedAchievementsList = $this->getUnlockedAchievementsList($userDataWithLessonAndComments);
        $totalAchievements = sizeof($getUnlockedAchievementsList);

        $unlockableBadgesWithAchievementThresholds = $this->getAllUnlockableBadgesWithAchievementThresholds();
        
        $badge = array_keys($unlockableBadgesWithAchievementThresholds,$totalAchievements);

        //dd($unlockableBadgesWithAchievementThresholds);

        foreach ($unlockableBadgesWithAchievementThresholds as $badge => $threshold) {
            if ($totalAchievements >= $threshold) {
                return $badge;
            }
        }

        return "Beginner";
    }

    /**
     * Calculate Next unlockable Badge by user
     */
    public function nextUnlockableBadge($userDataWithLessonAndComments){

        if ($this->checkBadgeUnlocked($userDataWithLessonAndComments)) {
            $unlockableBadges = $this->getAllUnlockableBadges();
            
            $badgeName = $this->calculateBadge($userDataWithLessonAndComments);

        }else{
            //$a = $this->calculateBadge();
            $badgeName = $this->getAllUnlockableBadges()[1];
            

        }
        return $badgeName;
    }
    /**
     * Calculate no of achievement user needs for next badge
     */
    public function calculateRemainingAchievementThresholdToUnlockNextBadge($nextUnlockableBadge,$combinedUnlockedAchievements){
        $unlockableBadgesWithAchievementThresholds = $this->getAllUnlockableBadgesWithAchievementThresholds();
        $nextUnlockableBadgeRequiredAchievementThreshold = $unlockableBadgesWithAchievementThresholds[$nextUnlockableBadge];
        $unlockedNoOfAchievements = sizeof($combinedUnlockedAchievements);
        $remainingAchievementThresholdToUnlockNextBadge = $nextUnlockableBadgeRequiredAchievementThreshold - $unlockedNoOfAchievements ;
        return $remainingAchievementThresholdToUnlockNextBadge;
    }
    /**
     * Unlock user Achievement and badge based on user activity
     */
    public function unlockAchievement($achievementName)
    {
        $userDataWithLessonAndComments = $this->getUserDataWithLessonAndComments(1);

        dd($userDataWithLessonAndComments->comments->count());
        
        // Fire AchievementUnlocked event
        event(new AchievementUnlocked($this,$achievementName));

        // Logic to check if the user has earned a new badge and fire BadgeUnlocked event if so
        if ($this->checkBadgeUnlocked($userDataWithLessonAndComments)) {
            $badgeName = $this->calculateBadge($userDataWithLessonAndComments);
            event(new BadgeUnlocked($this,$badgeName,$userDataWithLessonAndComments));
        }
    }
}
