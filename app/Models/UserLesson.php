<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UserLesson extends Model
{
    use HasFactory;

    protected $table = 'lesson_user';
    public $timestamps = false;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'lesson_id',
        'user_id',
        'watched',

    ];

    /**
     * Get the user that learnt the lesson.
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function getLessonsUserWatched($userId){
        return $this->where('user_id', $userId)->where('watched',1)->get();
    }
}
