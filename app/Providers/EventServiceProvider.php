<?php

namespace App\Providers;

use App\Events\AchievementUnlocked;
use App\Events\BadgeUnlocked;
use App\Events\CommentWritten;
use App\Events\LessonWatched;
use App\Listeners\UnlockAchievement;
use App\Listeners\UnlockBadge;
use App\Listeners\UnlockCommentAchievements;
use App\Listeners\UnlockLessonAchievements;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event to listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        AchievementUnlocked::class => [
            UnlockAchievement::class,
        ],
        BadgeUnlocked::class => [
            UnlockBadge::class
        ],
        LessonWatched::class => [
            UnlockLessonAchievements::class,
        ],
        CommentWritten::class => [
            UnlockCommentAchievements::class,
        ],
    ];

    /**
     * Register any events for your application.
     */
    public function boot(): void
    {
        //
    }

    /**
     * Determine if events and listeners should be automatically discovered.
     */
    public function shouldDiscoverEvents(): bool
    {
        return false;
    }
}
