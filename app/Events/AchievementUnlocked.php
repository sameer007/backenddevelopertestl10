<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class AchievementUnlocked
{
    use Dispatchable, SerializesModels;

    public User $user;
    public $achievement_name;
    /**
     * Create a new event instance.
     */
    public function __construct(User $user,string $achievement_name)
    {
        //
        $this->user = $user ;
        $this->achievement_name = $achievement_name ;
    }
}
