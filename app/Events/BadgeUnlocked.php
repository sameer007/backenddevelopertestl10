<?php

namespace App\Events;

use App\Models\User;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class BadgeUnlocked
{
    use Dispatchable, SerializesModels;

    public User $user;
    public $badge_name;
    /**
     * Create a new event instance.
     */
    public function __construct(User $user,string $badge_name)
    {
        //
        $this->user = $user ;
        $this->badge_name = $badge_name ;

    }
}
