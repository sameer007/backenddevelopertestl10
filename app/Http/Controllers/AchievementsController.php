<?php

namespace App\Http\Controllers;

use App\Events\AchievementUnlocked;
use App\Events\BadgeUnlocked;
use App\Events\CommentWritten;
use App\Events\LessonWatched;
use App\Listeners\UnlockAchievement;
use App\Models\Comment;
use App\Models\Lesson;
use App\Models\User;
use App\Models\UserLesson;
use Illuminate\Http\Request;

class AchievementsController extends Controller
{   
    /**
     * Handle Achievement Route
     */
    public function index($userId)
    {   
        $user = new User();
        $userDataWithLessonAndComments = $user->getUserDataWithLessonAndComments($userId);

        $unlockedAchievements = $user->getUnlockedAchievementsList($userDataWithLessonAndComments);
        
        $nextAvailableAchievements = $user->getNextAvailableAchievements($userDataWithLessonAndComments);
        $combinedUnlockedAchievements = array_merge($unlockedAchievements['userLessonAchievements'],$unlockedAchievements['userCommentAchievements']);
        //dd($unlockedAchievements);

        $currentBadge = $user->calculateBadge($userDataWithLessonAndComments);
        $nextUnlockableBadge = $user->nextUnlockableBadge($userDataWithLessonAndComments);

        $remainingAchievementThresholdToUnlockNextBadge = $user->calculateRemainingAchievementThresholdToUnlockNextBadge(
            $nextUnlockableBadge,
            $combinedUnlockedAchievements
        );
        return response()->json([
            'unlocked_achievements' => $unlockedAchievements,
            'next_available_achievements' => $nextAvailableAchievements,
            'current_badge' => $currentBadge,
            'next_badge' => $nextUnlockableBadge,
            'remaining_to_unlock_next_badge' => $remainingAchievementThresholdToUnlockNextBadge
        ]);
    }
}
