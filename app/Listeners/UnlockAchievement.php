<?php

namespace App\Listeners;

use App\Events\AchievementUnlocked;
use App\Events\BadgeUnlocked;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Session;

class UnlockAchievement
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(AchievementUnlocked $event): void
    {
        //
        Session::flash('achievement',$event->achievement_name);
        $userDataWithLessonAndComments = $event->user ;
        $userBadge = $event->user->calculateBadge($userDataWithLessonAndComments);

        $unlockedAchievements = $event->user->getUnlockedAchievementsList($userDataWithLessonAndComments);
        
        $combinedUnlockedAchievements = array_merge($unlockedAchievements['userLessonAchievements'],$unlockedAchievements['userCommentAchievements']);
        
        $nextUnlockableBadge = $event->user->nextUnlockableBadge($userDataWithLessonAndComments);

        $remainingAchievementThresholdToUnlockNextBadge = $event->user->calculateRemainingAchievementThresholdToUnlockNextBadge(
            $nextUnlockableBadge,
            $combinedUnlockedAchievements
        );
        //dd($userBadge);
        if($remainingAchievementThresholdToUnlockNextBadge == 0){
            event(new BadgeUnlocked($event->user,$userBadge));
        }
    }
}
