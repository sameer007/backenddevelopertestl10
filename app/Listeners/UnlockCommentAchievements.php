<?php

namespace App\Listeners;

use App\Events\AchievementUnlocked;
use App\Events\CommentWritten;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use User as GlobalUser;

class UnlockCommentAchievements
{
    //public User $user ;
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        
        
    }

    /**
     * Handle the event.
     */
    public function handle(CommentWritten $event): void
    {
        //

        $commentsWritten = $event->comment->count();

        // Check and unlock relevant achievements
        $unlockedAchievement = $event->user->calculateUnlockedAchievement($event->user->commentAchievements,$commentsWritten);
        
        event(new AchievementUnlocked($event->user,$unlockedAchievement[0]));

    }
}
