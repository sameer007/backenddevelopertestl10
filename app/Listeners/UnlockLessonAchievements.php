<?php

namespace App\Listeners;

use App\Events\AchievementUnlocked;
use App\Events\LessonWatched;
use App\Models\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;

class UnlockLessonAchievements
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(LessonWatched $event): void
    {
        //
        $lessonsWatched = $event->lesson->count();

        // Check and unlock relevant achievements
        $unlockedAchievement = $event->user->calculateUnlockedAchievement($event->user->lessonAchievements,$lessonsWatched);
        
        event(new AchievementUnlocked($event->user,$unlockedAchievement[0]));
        
    }
}
