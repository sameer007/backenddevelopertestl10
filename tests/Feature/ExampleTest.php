<?php

namespace Tests\Feature;

use App\Events\BadgeUnlocked;
use App\Events\CommentWritten;
use App\Events\LessonWatched;
use App\Listeners\UnlockBadge;
use App\Listeners\UnlockCommentAchievements;
use App\Listeners\UnlockLessonAchievements;
use App\Models\Comment;
use App\Models\Lesson;
use App\Models\User;
use App\Models\UserLesson;
use Illuminate\Support\Facades\Event;
// use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    protected $userId ;
    /**
     * Test achievement route.
     */
    public function test_checkIfAchievementRouteWorks(): void
    {
        $userData = User::first();

        if(!isset($userData) || empty($userData)){
            $userData = User::factory()->create()->first();
        }
        $response = $this->get("/users/{$userData->id}/achievements");

        $this->assertTrue(isset($userData));
        $response->assertStatus(200);
    }
    /**
     * Test Listener for CommentWritten Event.
     */
    public function test_listener_is_attached_to_CommentWritten_Event()
    {
        Event::fake();
        Event::assertListening(
            CommentWritten::class,
            UnlockCommentAchievements::class
        );
    }
    /**
     * Test Listener for LessonWatched Event.
     */
    public function test_listener_is_attached_to_LessonWatched_Event()
    {
        Event::fake();
        Event::assertListening(
            LessonWatched::class,
            UnlockLessonAchievements::class
        );
    }

    /**
     * Test Listener for BadgeUnlocked Event.
     */
    public function test_listener_is_attached_to_BadgeUnlocked_Event()
    {
        Event::fake();
        Event::assertListening(
            BadgeUnlocked::class,
            UnlockBadge::class
        );
    }
    /**
     * Test retreivel of user data with user's lesson watched and comment written
     */
    public function test_getUserDataWithLessonAndComment()
    {
        
        $user = User::first();
        //$user = User::where('id',$this->userId)->first();
        $userDataWithLessonAndComments = $user->getUserDataWithLessonAndComments($user->id);
        $this->assertTrue(isset($userDataWithLessonAndComments));
    }
    /**
     * Test When new comment is added
     * Test When CommentWritten event is triggered
     */
    public function test_addedNewCommentEvent(): void
    {
        Event::fake();
        
        $user = User::first();

        $comment = new Comment();
        $addedNewComment = $comment->addNewComment($user->id);
        $recentlyAddedComment = $comment->getUserComment($user->id);
        $userDataWithLessonAndComments = $user->getUserDataWithLessonAndComments($user->id);
        
        event(new CommentWritten($recentlyAddedComment,$userDataWithLessonAndComments));
        
        Event::assertDispatched(CommentWritten::class);
        $this->assertTrue(isset($addedNewComment));
        $this->assertTrue(isset($recentlyAddedComment));
        $this->assertTrue(isset($userDataWithLessonAndComments));

    }
    /**
     * Test When new lesson is watched by user
     * Test When LessonWatched event is triggered
     */
    public function test_LessonWatchedEvent(): void
    {
        Event::fake();
        
        $user = User::first();

        $lesson = new Lesson();
        $userLesson = new UserLesson();

        $addedNewlesson = $lesson->addNewLesson($user->id);
        $recentlyWatchedLesson = $userLesson->getLessonsUserWatched($user->id);
        $userDataWithLessonAndComments = $user->getUserDataWithLessonAndComments($user->id);
        
        event(new CommentWritten($recentlyWatchedLesson,$userDataWithLessonAndComments));
        
        Event::assertDispatched(CommentWritten::class);
        $this->assertTrue(isset($addedNewlesson));
        $this->assertTrue(isset($recentlyWatchedLesson));
        $this->assertTrue(isset($userDataWithLessonAndComments));

    }

    /**
     * 
     * Test When BadgeUnlocked event is triggered
     */
    public function test_BadgeUnlockedEvent(): void
    {
        Event::fake();
        
        $user = User::first();
        
        $userDataWithLessonAndComments = $user->getUserDataWithLessonAndComments($user->id);
        
        $userBadge = $user->calculateBadge($userDataWithLessonAndComments);
        event(new BadgeUnlocked($userDataWithLessonAndComments,$userBadge));
        
        Event::assertDispatched(BadgeUnlocked::class);
        $this->assertTrue(isset($userDataWithLessonAndComments));

    }
}
